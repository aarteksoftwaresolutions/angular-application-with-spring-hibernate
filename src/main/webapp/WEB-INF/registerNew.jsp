<!-- <script src= "resources/angular/angular.min.js"></script>
<script src= "resources/angular/angular.js"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.6/angular.js"></script> -->
<div class="content">
    <h2 style="margin-top:50px;">Registration</h2>	
	<div class="section group">	
	  					
		<div class="col span_2_of_3">
		  <form ng-app="myApp" ng-controller="validateCtrl" name="myForm" novalidate method="POST" action="registrationAction"   autocomplete="off">
			  <div class="contact-form">
			  	    <h3>Your Personal Details</h3>	
				    
				    	<div>
					    	<!-- <span><label>NAME</label></span>
					    	<span><input type="text" value="" /></span> -->
					    	<p>NAME<br>
<input type="text" name="name" ng-model="name" required>
<span style="color:red" ng-show="myForm.name.$dirty && myForm.name.$invalid">
<span ng-show="myForm.name.$error.required">Customer Name is required.</span>
<span ng-show="myForm.name.$error.name">Invalid Name.</span>
</span>
					    </div>
					    <div>
					    	<span><label>E-MAIL</label></span>
					    	<input type="email" name="email" ng-model="email" required>
<span style="color:red" ng-show="myForm.email.$dirty && myForm.email.$invalid">
<span ng-show="myForm.email.$error.required">Username is required.</span>
<span ng-show="myForm.email.$error.email">Invalid Username address.</span>
</span>
					    </div>
					    <div>
					     	<span><label>TELEPHONE</label></span>
					    	<!-- <span><input type="text" value="" /></span> -->
					    	<div  ng-class="{'has-error': myForm.phone.$error.number}">
					    	 <input type="text"  ng-minlength="10" ng-maxlength="10"  id="inputPhone" name="phone" placeholder="Phone" ng-model="phone" ng-required="true">
            <span style="color:red" ng-show="myForm.phone.$error.required || myForm.phone.$error.number">Valid phone number is required</span>
            <span style="color:red" ng-show="((myForm.phone.$error.minlength || myForm.phone.$error.maxlength) && myForm.phone.$dirty) ">phone number should be 10 digits</span>
					    </div></div>
					    <div>
					    	<span><label>FAX</label></span>
					    	<span><input type="text" value="" /></span>
					    </div>
					   
	     	    
		    </div>
  		
			  <div class="contact-form">
				    <h3>Your Address</h3>	
					
						<div>
							<span><label>COMPANY</label></span>
							<span><input type="text" value="" /></span>
						</div>
						<div>
							<span><label>ADDRESS 1</label></span>
							<span><input type="text" value="" /></span>
						</div>
						<div>
							<span><label>ADDRESS 2</label></span>
							<span><input type="text" value="" /></span>
						</div>
						<div>
							<span><label>CITY</label></span>
							<span><input type="text" value="" /></span>
						</div>
						<div>
							<span><label>POST CODE</label></span>
							<span><input type="text" value="" /></span>
						</div>
						<div>
							<span><label>Country</label></span>
							<span><select name="country_id">
				<option value=""> --- Please Select --- </option>
												<option value="244">Aaland Islands</option>
																<option value="1">Afghanistan</option>
																<option value="2">Albania</option>
																<option value="3">Algeria</option>
																<option value="4">American Samoa</option>
																<option value="5">Andorra</option>
											  </select></span>
						</div>
						
					   
				
			</div>
		
			  <div class="contact-form">
				    <h3>Your Password</h3>	
					
						<div>
							<span><label>PASSWORD</label></span>
						   	<input type="password" name="password" ng-model="password" required>
                             <span style="color:red" ng-show="myForm.password.$dirty && myForm.password.$invalid">
                             <span ng-show="myForm.password.$error.required">Password is required.</span>
                             </span>
						</div>
						<div>
							<span><label>CONFIRM PASSWORD</label></span>
						   	<input type="password" name="cPassword" ng-model="cPassword" required id="confirmPassword">
                             <span style="color:red" ng-show="myForm.cPassword.$dirty && myForm.cPassword.$invalid">
                             <span ng-show="myForm.cPassword.$error.required">Confirm Password is required.</span>
                             </span>
						</div>
					   
				
				<div class="contact-form">
					<h3>Newsletter</h3>	
					
						<div>
							<span><label>Subscribe</label></span>
							<span style="float:left;"><input type="radio" value="yes" name="newsletter" />Yes</span>
							<span style="float:left; margin-left:10px;"><input type="radio" value="no" name="newsletter" />No</span>
						</div>
					   
				
			</div>
		</div>	
		<div class="contact-form clear" style="margin-right:10px;">
			<span>I have read and agree to the <a class="agree" href="https://www.sopnil.com/index.php?route=information/information/agree&amp;information_id=3"><b>Privacy Policy</b></a>                        <input type="checkbox" value="1" name="agree">
						&nbsp;
			<input type="submit" class="button btn btn-theme-default" value="Continue"><input type="submit" value="Register"></span>
		</div>
		</form>	
		</div>
		<div class="col span_1_of_3">
		  <div class="sidebar">
					  <h2>Account</h2>
					  <style>
					  .nav1 li{ display:block;  }
					  </style>
					  <ul class="nav1">
					<li><a href="login.html">Login</a></li>
		  <li><a href="register.html">Register</a></li> 
		  <li><a href="#">Forgotten Password</a></li>
		  
		  <li><a href="#">My Account</a></li>

		  
		  <li><a href="#">Address Book</a></li> 
		  <li><a href="#">Wish List</a></li> 
		  <li><a href="#">Order History</a></li>
		  <li><a href="#">Downloads</a></li> 
		  <li><a href="#">Reward Points</a></li> 
		  <li><a href="#">Returns</a></li> 
		  <li><a href="#">Transactions</a></li> 
		  <li><a href="#">Newsletter</a></li>
		  <li><a href="#">Recurring payments</a></li>

				  </ul>
					</div>
		</div>		
	</div>						
</div>
<script>
var app = angular.module('myApp', []);
app.controller('validateCtrl', function($scope) {
   /*  $scope.user = 'John Doe';
    $scope.email = 'john.doe@gmail.com'; */
});
</script>
<script>
function confirmPassword(){
	alert("hi");
}
</script>