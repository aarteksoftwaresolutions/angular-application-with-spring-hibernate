var app=angular.module('sandeepApp',['ngRoute']);
//alert("sandeep");
app.config(['$routeProvider',function($routeProvider){
      $routeProvider
          .when('/',{
                templateUrl: 'resources/pages/home.html'
          })
          .when('/login',{
                templateUrl: 'resources/pages/login.html',
                 controller: 'loginCtrl'
          })
          .when('/CustomerDetails',{
        	     controller: 'CustomerDetailsCtrl',
                 templateUrl: 'resources/pages/CustomerDetails.html'
                
          })
           .when('/editCusto/:custInfo',{
        	   controller: 'RegisterCtrl',
        	   templateUrl: 'resources/pages/registration.html'
          })
          .when('/register',{
              templateUrl: 'resources/pages/registration.html',
              controller: 'RegisterCtrl'
              
             
        });
        
}]);

/*This controller is for validations*/
app.controller('sandeepCtrl',function($scope){
});
