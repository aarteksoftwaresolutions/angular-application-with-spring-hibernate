app.controller('RegisterCtrl', function($scope, $location, $rootScope, $routeParams, $http,registrationService){
	$scope.custInfo = $routeParams.custInfo;
	if($scope.custInfo!=null){
    var registrationId=$scope.custInfo;
    $http.get('http://localhost:8080/acudraStore/editCustInfo/'+registrationId).
    success(function(data){
    	 registrationService.editDetails($scope,data);
    });
	}
    //=====================this is for drop down
   	 $http.get('http://localhost:8080/acudraStore/testGet').
        success(function(data){
            $scope.country = data.countryList;
        });
   	 //=========================================end and below start for submit registration form
	$scope.registerUser = function() {
		var data ={
			email:$scope.email,
			name:$scope.name,
			telephone:$scope.telephone,
			fax:$scope.fax,
			city:$scope.city,
			password:$scope.password,
			country:$scope.country,
			address1:$scope.address1,
			registrationId:$scope.registrationId
		};
		var res = $http.post('/acudraStore/registrationAction', data);
		res.success(function(data, status, headers, config) {
			alert(data.editInfo+"!!");
			   $scope.message = data.editInfo;
			   $location.path('/CustomerDetails'); 
			  });
	}
  // ==============================================end  
});