app.controller('CustomerDetailsCtrl', function($scope, $location, $rootScope,$routeParams, $http,services){
   $http.get('http://localhost:8080/acudraStore/customerDetails').
   success(function(data){
 	services.details($scope,$http,data);
   });
   $scope.deleteCustomerDetails = function(registrationId) {
	   $http.get('http://localhost:8080/acudraStore/deleteCustInfo/'+registrationId).
       success(function(data){
    	   alert(data.response+"!!");
    	   $location.path('/'); 
       });
   }
   $scope.editCustomerDetails = function(custInfo) {
	   $scope.custoInfo=custInfo.name;
	   $location.path('/editCusto/'+custInfo.registrationId);  
   }
   
});