package com.acudra.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.map.util.JSONPObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.acudra.model.Country;
import com.acudra.model.Registration;
import com.acudra.service.LoginService;
import com.fasterxml.jackson.databind.jsonFormatVisitors.JsonObjectFormatVisitor;

@Controller
public class LoginController {
	@Autowired
	LoginService loginService;

	/**
	 * show welcome page
	 * 
	 * @return
	 */
	@RequestMapping(value = { "/welcome" }, method = RequestMethod.GET)
	public String defaultPage() {

		return "AngularJs";
	}

	@RequestMapping(value = "/testGet", method = RequestMethod.GET)
	public @ResponseBody Map<Object, Object> testGo(ModelMap model) {
		System.out.println("testing");
		List<Country> countryList = null;
		countryList = loginService.getCountryName();
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("response", "Invoice has created");
		map.put("countryList", countryList);
		return map;
	}

	@RequestMapping(value = "/login/{email}/{password}", method = RequestMethod.POST)
	public @ResponseBody Map<Object, Object> login(@PathVariable("email") String email,
					@PathVariable("password") String password, ModelMap model) {
		/*
		 * List<Country> countryList= null; countryList=
		 * loginService.getCountryName();
		 * System.out.println("values of contry"+countryList
		 * .get(0).getCountryName()); model.put("contry", countryList);
		 * model.put("Registration", new Registration()); model.put("Country",
		 * new Country());
		 */
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("response", "Welcome to new Angular application created by patidar");
		return map;

	}

	@RequestMapping(value = "/customerLogin", method = RequestMethod.POST)
	public String customerLogin(@ModelAttribute("Registration") Registration registration, ModelMap model)
					throws Exception {
		boolean login = false;
		if (registration != null) {
			/*if (registration.getEmail() != null && registration.getPassword() != null) {
				login = loginService.customerLogin(registration);
			}*/
		}
		if (login == false) {
			model.addAttribute("invalid", "Invalid user name and password");
			return "login";
		}
		return "home";

	}

	@RequestMapping(value = "/ajLogin", method = RequestMethod.GET)
	public String ajLoginView(Map<String, Object> map) {
		map.put("Registration", new Registration());
		return "ajLogin";
	}

	@RequestMapping(value = "/ajCustLogin", method = RequestMethod.POST)
	public String ajcustomerLogin(@ModelAttribute("Registration") Registration registration, ModelMap model)
					throws Exception {

		model.addAttribute("invalid", "Sorry This is Only for Demo");
		return "ajLogin";

	}
}
