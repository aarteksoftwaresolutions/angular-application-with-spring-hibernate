package com.acudra.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.acudra.model.Country;
import com.acudra.model.Registration;
import com.acudra.service.RegistrationService;

@Controller
public class RegistrationController {

	@Autowired
	RegistrationService registrationService;

	/**
	 * show welcome page
	 * 
	 * @return
	 */
	@RequestMapping(value = "/registrationView", method = RequestMethod.GET)
	public String registrationPage(Map<String, Object> map) {
		map.put("Registration", new Registration());
		return "registerNew";
	}
	/*@RequestMapping(value = "/registrationAction", method = RequestMethod.POST)
	public String saveCandidate(@ModelAttribute("Registration") Registration registration) throws Exception {
		registrationService.saveRegistrationForm(registration);
		System.out.println("done");
		return "registerNew";
	}*/
	@RequestMapping(value = "/registrationAction", method = RequestMethod.POST)
	public @ResponseBody Map<Object, Object> login(@RequestBody Registration registration, ModelMap model) {
		Map<Object, Object> map = new HashMap<Object, Object>();
		if(registration.getRegistrationId()!=null){
			registrationService.editRegistrationForm(registration);
			map.put("editInfo", "Record Successfully Updated");
			return map;
		}else{
		registrationService.saveRegistrationForm(registration);
		map.put("editInfo", "Registration form is successfully saved");
		return map;
		}

	}
	@RequestMapping(value = "/customerDetails", method = RequestMethod.GET)
	public @ResponseBody Map<Object, Object> testGo(ModelMap model) {
		System.out.println("testing");
		List<Registration> detailsList = null;
		detailsList = registrationService.getCustomerDetails();
		System.out.println("===="+detailsList.get(0).getRegistrationId());
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("detailsList", detailsList);
		return map;
	}
	
	@RequestMapping(value = "/deleteCustInfo/{registrationId}", method = RequestMethod.GET)
	public @ResponseBody Map<Object, Object> deleteCustInfo(@PathVariable("registrationId") Integer registrationId, ModelMap model) {
		registrationService.deleteCustomerDetails(registrationId);
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("response", "Record deleted successfully");
		return map;

	}
	@RequestMapping(value = "/editCustInfo/{registrationId}", method = RequestMethod.GET)
	public @ResponseBody Map<Object, Object> editCustInfo(@PathVariable("registrationId") Integer registrationId, ModelMap model) {
		List custDetailsList=null;
		custDetailsList= registrationService.editCustomerDetails(registrationId);
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("custDetailsList",custDetailsList);
		return map;

	}
}
