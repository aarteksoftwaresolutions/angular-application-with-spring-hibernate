package com.acudra.repositoryImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.HibernateTemplate;
import org.springframework.stereotype.Repository;

import com.acudra.model.Registration;
import com.acudra.repository.RegistrationRepository;

@Repository("registrationRepository")
public class RegistrationRepositoryImpl implements RegistrationRepository {

	@Autowired
	private HibernateTemplate hibernateTemplate;

	public Boolean saveRegistrationForm(Registration registration) {
		hibernateTemplate.save(registration);
		return true;
	}

	@SuppressWarnings("unchecked")
	public List<Registration> getCustomerDetails() {
		 List<Registration> detailsList = null;
		 detailsList =  (List<Registration>) hibernateTemplate.find("from Registration");
		return detailsList;
	}

	public void deleteCustomerDetails(Integer registrationId) {
	Registration registration= new Registration();
	registration.setRegistrationId(registrationId);
	hibernateTemplate.delete(registration);
	//hibernateTemplate.delete("DELETE FROM Registration res WHERE res.REGISTRATION_ID="+registrationId);
	}

	public List editCustomerDetails(Integer registrationId) {
		List custDetailsList = null;
		custDetailsList= hibernateTemplate.find("from Registration where REGISTRATION_ID="+registrationId);
		return custDetailsList;
	}

	public Boolean editRegistrationForm(Registration registration) {
		if(registration!=null){
		hibernateTemplate.saveOrUpdate(registration);
		return true;
		}else{
			return null;
		}
	}
}
