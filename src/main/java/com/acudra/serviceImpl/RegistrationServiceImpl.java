package com.acudra.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.acudra.model.Country;
import com.acudra.model.Registration;
import com.acudra.repository.RegistrationRepository;
import com.acudra.service.RegistrationService;

@Service("registrationService")
@Transactional
public class RegistrationServiceImpl implements RegistrationService {

	@Autowired
	private RegistrationRepository registrationRepository;

	public Boolean saveRegistrationForm(Registration registration) {
		registrationRepository.saveRegistrationForm(registration);
		return true;
	}
	/*
	 * @Autowired private RegistrationRepository registrationRepository;
	 */

	public List<Registration> getCustomerDetails() {
		List<Registration> detailsList = new ArrayList<Registration>();
		detailsList = registrationRepository.getCustomerDetails();
		return detailsList;
		
	}

	public void deleteCustomerDetails(Integer registrationId) {
		registrationRepository.deleteCustomerDetails(registrationId);
		
	}

	@SuppressWarnings("rawtypes")
	public List editCustomerDetails(Integer registrationId) {
		List custDetailsList = new ArrayList();
		custDetailsList= registrationRepository.editCustomerDetails(registrationId);
		return custDetailsList;
	}
	public Boolean editRegistrationForm(Registration registration) {
		registrationRepository.editRegistrationForm(registration);
		return true;
	}

}
