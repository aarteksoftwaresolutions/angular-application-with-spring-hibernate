package com.acudra.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acudra.model.Country;
import com.acudra.model.Registration;
import com.acudra.repository.LoginRepository;
import com.acudra.service.LoginService;

@Service("")
public class LoginServiceImpl implements LoginService {

	@Autowired
	LoginRepository loginRepository;

	public Boolean customerLogin(Registration registration) {

		return loginRepository.customerLogin(registration);

	}

	public List<Country> getCountryName() {
		List<Country> countryList = new ArrayList<Country>();
		countryList = loginRepository.getCountryName();
		return countryList;
	}

}
