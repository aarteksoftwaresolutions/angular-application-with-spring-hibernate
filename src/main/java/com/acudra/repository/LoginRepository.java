package com.acudra.repository;

import java.util.List;

import com.acudra.model.Country;
import com.acudra.model.Registration;

public interface LoginRepository {

	public Boolean customerLogin(Registration registration);

	public List<Country> getCountryName();

}
