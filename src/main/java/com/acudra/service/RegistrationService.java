package com.acudra.service;

import java.util.List;

import com.acudra.model.Registration;

public interface RegistrationService {

	public Boolean saveRegistrationForm(Registration registration);

	public List<Registration> getCustomerDetails();

	public void deleteCustomerDetails(Integer registrationId);

	public List editCustomerDetails(Integer registrationId);

	public Boolean editRegistrationForm(Registration registration);
}
